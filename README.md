# Examples
This is a collection of hybrid models (here we mean a combination of data-driven and mechanistic) for the field of water and wastewater treatment.
The managagers are from the IWA's working group on hybrid modelling. https://gitlab.com/hm_wg/homepage

## List of examples
- Title: Application. Hybrid Model Type. Who did it?

## How can I get my example to this repository?
Please follow the standard format of example X.
Every example will have a folder in the repository.
It needs an executable part and a description that are separate.
Criteria:
- There needs to be an open repository hosting the main package.
- There needs to be an executable part that can be uploaded her as an example.
- Needs a documentation with the executable example as a description that runs stanalone of proprietary software.
- Needs a license.
- The example (not the entire project that the example is taken from) needs a license to make sure how it can be used. Needs to allow to be reused. e.g. cc-by or GPL needs to be approved by OSI. https://opensource.org/licenses

Description of the example needs to include the following points:
- What was the insentive to choose a hybrid model?
- What is the hybrid model form chosen and why was it chosen?
- Would there have been a simpler model?

Examples need to fullfill the following criteria:
- An example will be treated as a submodule: https://git-scm.com/book/en/v2/Git-Tools-Submodules
- They need to be reproducible.
- They need to be well documented.
- Standalone (one can use a generated input that is clearly defined, but is stated how it was created, specifies which model, reactor configuration, etc. but preferably create a synthetic curve).
- Comparison with real (or simulated real) data should be provided.
- At least one plot needs to be generated.

## Review

## Upload the files
Please create a merge request with the example that fullfills the criteria.
This will then be reviewed.
Until the process is established please first talk to the working group before you invest time into creating an example.

## License
Please check the subprojects for their own licenses.

